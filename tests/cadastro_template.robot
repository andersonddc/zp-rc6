***Settings***
Documentation       Cadastro de clientes

Resource            ../resources/base.robot

Test Setup          Login Session
Test Teardown       Finish Session

Test Template       Cadastrar novo cliente

***Keywords***
Cadastrar novo cliente
    [Arguments]         ${name}     ${cpf}      ${address}      ${phone_number}     ${output_mensagem}

    Dado que acesso o formulário de cadastro de clientes
    Quando faço a inclusão desse cliente:
    ...         ${name}     ${cpf}      ${address}      ${phone_number}
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios      ${output_mensagem}


***Test Cases***
Nome obrigatório            ${EMPTY}            60000000320         Rua A, 01       85999999999             Nome é obrigatório
Cpf obrigatório             Anderson Dantas     ${EMPTY}            Rua A, 01       85999999999             CPF é obrigatório
Endereço obrigatório        Anderson Dantas     60000000320         ${EMPTY}        85999999999             Endereço é obrigatório
Telefone obrigatório        Anderson Dantas     60000000320         Rua A, 01       ${EMPTY}                Telefone é obrigatório


# Cenários
# Nome é Obrigatório
# Cpf é Obrigatório
# Endereço é Obrigatório
# Telefone é Obrigatório

# 1o Desafio do RoboCamp

# Implementar os cenários de campos obrigatórios usando a técnica do Test Template
# Premio: Os 10 Primeiros que entregarem até o dia 01 de Setembro de 2020 até as 19hs, ganharaão
# uma vaga no curso Performance Tests (https://cursos.qaninja.com.br/performance)

# Entrega: Subir o seu código no gitlab e enviar o link nos comentários dessa video aula!